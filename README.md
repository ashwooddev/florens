### Florens

---

#### req:

- `node v.7+`
- `gulp v3+`

#### run:

- `npm install`
- `gulp watch`

#### stack:

- php
- jQuery
- gulp
- bootstrap v3.3.x
- scss
- pug
