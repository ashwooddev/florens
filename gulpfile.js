'use strict';
const   gulp            = require('gulp'),
        sass            = require('gulp-sass'),
        browserSync     = require('browser-sync'),
        autoprefixer    = require('gulp-autoprefixer'),
        sourcemaps      = require('gulp-sourcemaps'),
        pug             = require('gulp-pug');

// gulp.task('mytask', () => {
//   return gulp.src('source-files') // Выборка исходных файлов для обработки плагином
//     .pipe(plugin()) // Вызов Gulp плагина для обработки файла
//     .pipe(gulp.dest('folder')) // Вывод результирующего файла в папку назначения (dest - пункт назначения)
// })

gulp.task('scss', () => {
    gulp
        .src('app/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 9'], { cascade: true }))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('pug', () => {
    gulp
        .src('app/views/*.pug')
        .pipe(pug({
            pretty : true // uncompressed html output
        }))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('app/'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('make-news', () => {
    gulp
        .src('app/views/news/*.pug')
        .pipe(pug({
            pretty : true // uncompressed html output
        }))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('app/'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', () => {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});

gulp.task('watch', ['browser-sync', 'scss', 'pug', 'make-news'], () => {
    gulp.watch('app/scss/**/*.scss', ['scss']);
    gulp.watch('app/views/**/*.pug', ['pug', 'make-news']);
    gulp.watch('app/**/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('default', ['watch']);
