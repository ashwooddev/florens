<?php
// php
$errors         = array();      // array to hold validation errors
$data           = array();      // array to pass back data
// validate the variables ======================================================
    // if any of these variables don't exist, add an error to our $errors array
    if (empty($_POST['name']))
        $errors['name'] = 'Name is required.';
    if (empty($_POST['email']))
        $errors['email'] = 'Email is required.';
    if (empty($_POST['subject']))
        $errors['subject'] = 'Subject is required.';
    if (empty($_POST['message']))
        $errors['message'] = 'Message is required.';
    // return a response ===========================================================
    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($errors)) {
        // if there are items in our errors array, return those errors
        $data['success'] = false;
        $data['message'] = 'Возникла ошибка!';
        $data['errors']  = $errors;
    } else {
        // if there are no errors process our form, then return a message
        // DO ALL YOUR FORM PROCESSING HERE
        $to  = "info@florens.by";
        $subject = htmlspecialchars($_POST['subject']);
        //$subject = "=?koi8-r?B?8M/e1M/Xz8Ug1dfFxM/NzMXOycU=?=";
        $message = '
        <html>
            <head>
                <title>Уведомление с сайта</title>
            </head>
            <body>
                <h3>Уведомление с сайта</h3>
                <p><b>Тема сообщения:</b> '.htmlspecialchars($_POST['subject']).'</p>
                <p><b>Вам пишет:</b> '.htmlspecialchars($_POST['name']).'</p>
                <p><b>Cообщение:</b> '.htmlspecialchars($_POST['message']).'</p>
            </body>
        </html>';
        // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';
        // Additional headers
        $headers[] = 'From: <'.htmlspecialchars($_POST['email']).'>';
        // THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)
        mail($to, $subject, $message, implode("\r\n", $headers));
        // if(!$send) {
        //     $errors['send'] = 'Sending error.';
        //     $data['success'] = false;
        //     $data['errors']  = $errors;
        //     return;
            // show a message of success and provide a true success variable
            $data['success'] = true;
            $data['message'] = 'Ваше сообщение успешно отправлено!';
    }
    // return all our data to an AJAX call
    echo json_encode($data);


// if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message'])) {
//     $to  = "v.bozhkov@ashwood.by";

//     $subject = "=?koi8-r?B?8M/e1M/Xz8Ug1dfFxM/NzMXOycU=?=";

//     $message = '
//     <html>
//         <head>
//             <title>Уведомление с сайта от '.htmlspecialchars($_POST['name']).'</title>
//         </head>
//         <body>
//             <h3>Уведомление с сайта от '.htmlspecialchars($_POST['name']).'</h3>
//             <p>'.htmlspecialchars($_POST['message']).'</p>
//         </body>
//     </html>';

//     // To send HTML mail, the Content-type header must be set
//     $headers[] = 'MIME-Version: 1.0';
//     $headers[] = 'Content-type: text/html; charset=iso-8859-1';
//     // Additional headers
//     $headers[] = 'From: '.htmlspecialchars($_POST['name']).' <'.htmlspecialchars($_POST['email']).'>';

//     return json_encode( array( 'success' => mail($to, $subject, $message, implode("\r\n", $headers)) ) );
// } else {
//     return json_encode( array( 'success' => 'false' ) );
// }
