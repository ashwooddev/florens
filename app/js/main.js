'use stric';

$( document ).ready(function() {
    var year = new Date();
    $('footer .copy span').text(year.getFullYear());

    if(document.location.search.match(/\?load\=/)) {
        $('li.active').removeClass('active');
        loadPage(document.location.search.replace(/\?load\=/, ''));
        var links = $('.page-navigation-ash');
        for(var i = 0; i < links.length; i++)
            if(links[i].getAttribute('href').replace(/page\.html\?load\=/, '') === document.location.search.replace(/\?load\=/, ''))
                links[i].parentNode.classList.add('active');
    }
// < SEARCH >
    // $('#main-search-trigger').hover(function () {
    //  $('#main-search-input').removeClass('hidden');
    // },
    // function () {
    //  $('#main-search-input').addClass('hidden');
    // });
    // $('#main-search-input').hover(function () {
    //  $('#main-search-input').removeClass('hidden');
    // },
    // function () {
    //  $('#main-search-input').addClass('hidden');
    // });
// < / SEARCH >

    document.getElementById('search-button').addEventListener('click', function(){
        document.getElementById('search-form').classList.remove('hide');
        setTimeout(function() {
            document.getElementById('search-form').style.opacity = 1;
        }, 250);
    })

    document.getElementById('search-close').addEventListener('click', function(){
        document.getElementById('search-form').style.opacity = 0;
        setTimeout(function() {
            document.getElementById('search-form').classList.add('hide');
        }, 750);
    })


    $('.page-navigation-ash').click(function() {
        if(document.location.pathname.match(/page\.html/)) {
            var page = $(this).attr('href').replace(/page\.html\?load\=/, '');
            $('.preloader#preloader').removeClass('hide');
            $('li.active').removeClass('active');
            loadPage(page);
            $(this).parent().addClass('active');
            return false;
        }
    });
    // process the form
    $('.form-box').submit(function(event) {
        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'name'              : $('input[name=name]').val(),
            'email'             : $('input[name=email]').val(),
            'subject'           : $('input[name=subject]').val(),
            'message'           : $('textarea[name=message]').val()
        };
        $('#waitingpreloader').removeClass('notshow');
        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'send.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
            // using the done promise callback
            .done(function(data) {
                // log data to the console so we can see
                console.log(data);
                if(data.success) {
                  $('input[name=name]').val('');
                  $('input[name=email]').val('');
                  $('input[name=subject]').val('');
                  $('textarea[name=message]').val('');
                }
                setTimeout(function() {
                  $('.message').text(data.message);
                  $('.message').removeClass('notshow');
                  $('.science').addClass('notshow');
                  setTimeout(function(){
                    $('#waitingpreloader').addClass('notshow');
                    setTimeout(function(){
                      $('.message').text('');
                      $('.message').addClass('notshow');
                      $('.science').removeClass('notshow');
                    }, 300);
                  }, 1000);
                }, 1500);
                // here we will handle errors and validation messages
            });
        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });
});

function loadPage(page) {
    var html;
    console.log(page);
    switch (page) {
        case 'projects': html = '_1.html'; break;
        case 'services': html = '_2.html'; break;
        case 'create-project': html = '_3.html';  break;
        case 'contacts': html = '_4.html'; break;
        case 'sertificats': html = '_5.html'; break;
        case 'about': html = '_6.html'; break;
        default: html = '404.html'; break;
    }
    $.ajax({
        url: html,
        context: document.body,
        // type: 'POST',
        // url: 'lib/loadPage.php',
        // data: {page:page},
        dataType: 'html',
        success: function(data) {
            $('.preloader#preloader').addClass('hide');
            $('#data-page').empty();
            $('#data-page').append(data);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        },
    });
}
/*
if(isset($_POST['page'])) {
    switch ($_POST['page']) {
        case 'projects': include ('../_1.html'); break;
        case 'services': include ('../_2.html'); break;
        case 'create-project': include ('../_3.html');  break;
        case 'contacts': include ('../_4.html'); break;
        case 'sertificats': include ('../_5.html'); break;
        case 'about': include ('../_6.html'); break;
        default:
            echo '<div class="block-1 contacts">
        <div class="row">
            <div class="col-md-12 mat-75 mab-80">
                <h1 class="text-center">404 page not found</h1>
            </div>
        </div>
    </div>';
    }
}
*/
